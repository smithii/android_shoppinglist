package org.projects.shoppinglist;

import android.app.Application;

import com.firebase.client.Firebase;
import com.flurry.android.FlurryAgent;

/**
 * Created by Boshi on 09-05-2016.
 */
public class ShoppingListApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);

        new FlurryAgent.Builder()
                .withLogEnabled(false)
                .build(this, "4D5C85N78MY3DWF869KB");
    }
}
