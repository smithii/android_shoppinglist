package org.projects.shoppinglist;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import java.util.List;

/*
Example of use:

        Spinner colorSpinner = (Spinner) findViewById(R.id.colorSpinner);

        List clrList = new ArrayList();

        clrList.add(R.color.bright_pink);
        clrList.add(R.color.red);
        clrList.add(R.color.orange);
        clrList.add(R.color.yellow);
        clrList.add(R.color.chartreuse);
        clrList.add(R.color.green);
        clrList.add(R.color.spring_green);
        clrList.add(R.color.cyan);
        clrList.add(R.color.azure);
        clrList.add(R.color.blue);
        clrList.add(R.color.violet);
        clrList.add(R.color.magenta);

        CustomSpinnerAdapter clrAdapter = new CustomSpinnerAdapter(this, clrList);

        colorSpinner.setAdapter(clrAdapter);
*/

/**
 * https://stackoverflow.com/questions/13156264/android-dropdown-color-picker
 */
public class CustomSpinnerAdapter<Integer> extends ArrayAdapter implements SpinnerAdapter {

    private final List<Integer> objects; // android.graphics.Color list
    private final Context context;

    public CustomSpinnerAdapter(Context context, List<Integer> objects) {
        super(context, R.layout.activity_main, objects);

        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        //super.getDropDownView(position, convertView, parent);

        View rowView = convertView;


        if (rowView == null) {
            // Get a new instance of the row layout view
            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            rowView = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, null);

            rowView.setBackgroundColor(ContextCompat.getColor(context, objects.get(position).hashCode()));

        } else {
            rowView.setBackgroundColor(ContextCompat.getColor(context, objects.get(position).hashCode()));
        }


        return rowView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;


        if (rowView == null) {
            // Get a new instance of the row layout view
            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            rowView = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, null);

            rowView.setBackgroundColor(ContextCompat.getColor(context, objects.get(position).hashCode()));

        } else {
            rowView.setBackgroundColor(ContextCompat.getColor(context, objects.get(position).hashCode()));
        }


        return rowView;
    }
}