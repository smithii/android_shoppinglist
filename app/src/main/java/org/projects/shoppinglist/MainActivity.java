package org.projects.shoppinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.ui.FirebaseListAdapter;
import com.flurry.android.FlurryAgent;

public class MainActivity extends AppCompatActivity {

    Firebase fireRef;

    FirebaseListAdapter<Product> fireAdapter;

    ListView listView;
    Product lastDeletedProduct;
    int lastDeletedPosition;
    LinearLayout layoutParent;

    boolean isEditing;

    EditText nameEditor;
    EditText qtyEditor;

    // The view to be edited
    View editView;

    int editIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isEditing = false;

        //TODO: Keep editing when turning (portrait/landscape)
        /*if (savedInstanceState != null)
        {
            isEditing = savedInstanceState.getBoolean("isEditing");

            if (isEditing)
            {
                editView = listView.findViewById(savedInstanceState.getInt("editViewId"));
                editIndex = savedInstanceState.getInt("editIndex");

                if (editIndex >= 0)
                    listView.setItemChecked(editIndex, true);
            }
        }*/

        qtyEditor = (EditText) findViewById(R.id.qtyInput);
        nameEditor = (EditText) findViewById(R.id.itemInput);

        layoutParent = (LinearLayout)findViewById(R.id.parentPanel);

        fireRef = new Firebase("https://shoppinglist-smithii.firebaseio.com/items");

        fireAdapter = new FirebaseListAdapter<Product>
                (
                this,
                Product.class,
                android.R.layout.simple_list_item_checked,
                fireRef
                )
        {
            @Override
            protected void populateView(View view, Product product, int i) {
                TextView text = (TextView)view.findViewById(android.R.id.text1);
                text.setText(product.toString());
            }
        };


        //getting our listiew - you can check the ID in the xml to see that it
        //is indeed specified as "list"
        listView = (ListView) findViewById(R.id.list);

        //setting the adapter on the listview
        listView.setAdapter(fireAdapter);
        //here we set the choice mode - meaning in this case we can
        //only select one item at a time.
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        Button addButton = (Button) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( addProduct() )
                    FlurryAgent.logEvent("Product_Added");
            }
        });

        final Button editButton = (Button) findViewById(R.id.editButton);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editView((Button)v);
            }
        });

        Button delButton = (Button) findViewById(R.id.delButton);
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedPosition = listView.getCheckedItemPosition();

                if (checkedPosition >= 0) {
                    //Saves the deleted product
                    lastDeletedPosition = checkedPosition;
                    lastDeletedProduct = fireAdapter.getItem(checkedPosition);

                    //Deletes the product
                    FlurryAgent.logEvent("Product_Deleted");
                    fireAdapter.getRef(checkedPosition).setValue(null);

                    stopEditing(editButton);

                    listView.clearChoices();

                    //Snackbar
                    Snackbar sb = Snackbar.make(listView, "Item Deleted", Snackbar.LENGTH_LONG)
                            .setAction("UNDO", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    FlurryAgent.logEvent("Product_Deleted_Undo");
                                    fireRef.push().setValue(lastDeletedProduct);
                                    Snackbar snackbar = Snackbar.make(listView, "Item restored!", Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }
                            });

                    sb.show();
                }
            }
        });



        Spinner qtySpinner = (Spinner) findViewById(R.id.qtySpinner);
        qtySpinner.setOnItemSelectedListener(new ListView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                qtyEditor.setText(parent.getSelectedItem().toString());

                /*
                //TODO: Set current value to blank.
                parent.setOnItemSelectedListener(null);
                parent.setSelection(0);
                parent.setOnItemSelectedListener(this);*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                stopEditing(editButton);

                editView = view;
                editIndex = position;
            }
        });

        getPreferences();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id)
        {
            case R.id.action_clear_all:
                showDialog();
                break;
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivityForResult(intent, 1);
                break;
            case R.id.action_share:
                shareList();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);

        stopEditing((Button)findViewById(R.id.editButton));

        //TODO: Keep editing when turning (portrait/landscape)
        /*if (!isEditing)
            return;

        bundle.putBoolean("isEditing", isEditing);
        bundle.putInt("editViewId", editView.getId());
        bundle.putInt("editIndex", editIndex);*/
    }

    //This will be called when other activities in our application
    //are finished.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode==1) //exited our preference screen
        {
            getPreferences();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void getPreferences() {
        SharedPreferences prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
        String bgColor = prefs.getString("color", "");

        Log.d("clr", bgColor.toLowerCase());

        if (bgColor.equals(""))
            return;

        int c = ContextCompat.getColor(this, getResourceId(bgColor.toLowerCase(), "color", getPackageName()));

        layoutParent.setBackgroundColor(c);
    }

    // From https://stackoverflow.com/questions/4427608/android-getting-resource-id-from-string
    public int getResourceId(String pVariableName, String pResourcename, String pPackageName) {
        try {
            return getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    // Converts the shoppinglist to a string that we are able to share with another app
    public String listToString(){

        String result = "";

        for (int i = 0; i < fireAdapter.getCount(); i++)
        {
            Product p = fireAdapter.getItem(i);

            result += p.toString() + "\n";
        }

        if (result.equals(""))
            result = getString(R.string.emptyList);

        return result;
    }

    public void shareList() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, listToString() );
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void showDialog()
    {
        //showing our dialog.
        ClearListDialogFragment dialog = new ClearListDialogFragment()
        {
            @Override
            protected void positiveClick()
            {
                FlurryAgent.logEvent("List_Cleared");

                for (int i = 0; i < fireAdapter.getCount(); i++)
                {
                    fireAdapter.getRef(i).setValue(null);
                }
            }

            @Override
            protected void negativeClick()
            {
            }
        };

        //Here we show the dialog
        //The tag "MyFragement" is not important for us.
        dialog.show(getFragmentManager(), "MyFragment");
    }

    // Returns true if a product was successfully added to the list
    public boolean addProduct() {
        String productName = nameEditor.getText().toString();
        String qty = qtyEditor.getText().toString();

        if (qty.isEmpty() || qty.equals("0") || productName.equals(""))
            return false;

        fireRef.push().setValue(new Product(productName, Integer.parseInt(qty)));

        nameEditor.setText("");
        qtyEditor.setText("");

        return true;
    }

    public void stopEditing(Button editBtn)
    {
        if (!isEditing)
            return;

        nameEditor.setText("");
        qtyEditor.setText("");

        editBtn.setText(getText(R.string.edit));

        listView.setItemChecked(editIndex, false);

        editIndex = listView.getCheckedItemPosition();

        if (editView != null)
        {
            editView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            editView = null;
        }

        isEditing = false;
    }

    public void editView(Button editBtn)
    {
        if (editView == null)
        {
            //TODO: The text of the toast is black and fuzzy... Hotfix was to set a white background
            Toast t = Toast.makeText(this, getText(R.string.editError), Toast.LENGTH_SHORT);
            t.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            t.show();

            return;
        }

        if (isEditing)
        {
            String productName = nameEditor.getText().toString();
            String qty = qtyEditor.getText().toString();

            if (qty.isEmpty() || qty.equals("0") || productName.equals("")) {
                Toast.makeText(this, getString(R.string.editValueError), Toast.LENGTH_LONG).show();

                return;
            }

            fireAdapter.getRef(editIndex).setValue(new Product(productName, Integer.parseInt(qty)));

            stopEditing(editBtn);
        }
        else
        {
            isEditing = true;

            editBtn.setText(getText(R.string.done));

            Product pdr = fireAdapter.getItem(editIndex);

            editView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.yellow));

            nameEditor.setText(pdr.getName());
            qtyEditor.setText( String.valueOf( pdr.getQuantity() ));
        }
    }

}
